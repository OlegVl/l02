package ru.ovk.home.constant;

public class TerminalMessageConst {
        public static final String MHELP="помощь";
        public static final String MVERSION="версия";
        public static final String MERROR="ошибка";
        public static final String MABOUT="о программе";
        public static final String MDEFAULT="по умолчанию";
}
